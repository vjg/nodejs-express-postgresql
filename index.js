const express = require("express");
const app = express();
const PORT = process.env.PORT || 3000;

app.get("/", (req, res) => {
    res.send("Hello World!");
});

app.listen( PORT, function(err) {
    if(!err) console.log(`Started on port ${PORT}`);
    else console.error(err);
});